<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'outreach', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'outreach' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', __( 'Outreach Pro Theme', 'outreach' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/outreach/' );
define( 'CHILD_THEME_VERSION', '3.0.1' );

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Enqueue Google fonts
add_action( 'wp_enqueue_scripts', 'outreach_google_fonts' );
function outreach_google_fonts() {

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:400,700', array(), CHILD_THEME_VERSION );
	
}

//* Enqueue Responsive Menu Script
add_action( 'wp_enqueue_scripts', 'outreach_enqueue_responsive_script' );
function outreach_enqueue_responsive_script() {

	wp_enqueue_script( 'outreach-responsive-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0' );

}

//* Add new image sizes
add_image_size( 'home-top', 1140, 460, TRUE );
add_image_size( 'home-bottom', 285, 160, TRUE );
add_image_size( 'sidebar', 300, 150, TRUE );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'height'          => 100,
	'width'           => 340,
) );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for additional color style options
add_theme_support( 'genesis-style-selector', array(
	'outreach-pro-blue' 	=>	__( 'Outreach Pro Blue', 'outreach' ),
	'outreach-pro-orange' 	=> 	__( 'Outreach Pro Orange', 'outreach' ),
	'outreach-pro-purple' 	=> 	__( 'Outreach Pro Purple', 'outreach' ),
	'outreach-pro-red' 		=> 	__( 'Outreach Pro Red', 'outreach' ),
) );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'nav',
	'subnav',
	'site-inner',
	'footer-widgets',
	'footer',
) );

//* Add support for 4-column footer widgets
add_theme_support( 'genesis-footer-widgets', 4 );

//* Set Genesis Responsive Slider defaults
add_filter( 'genesis_responsive_slider_settings_defaults', 'outreach_responsive_slider_defaults' );
function outreach_responsive_slider_defaults( $defaults ) {

	$args = array(
		'location_horizontal'             => 'Left',
		'location_vertical'               => 'bottom',
		'posts_num'                       => '4',
		'slideshow_excerpt_content_limit' => '100',
		'slideshow_excerpt_content'       => 'full',
		'slideshow_excerpt_width'         => '35',
		'slideshow_height'                => '460',
		'slideshow_more_text'             => __( 'Continue Reading', 'outreach' ),
		'slideshow_title_show'            => 1,
		'slideshow_width'                 => '1140',
	);

	$args = wp_parse_args( $args, $defaults );
	
	return $args;
}

//* Hook after post widget after the entry content
add_action( 'genesis_after_entry', 'outreach_after_entry', 5 );
function outreach_after_entry() {

	if ( is_singular( 'post' ) )
		genesis_widget_area( 'after-entry', array(
			'before' => '<div class="after-entry widget-area">',
			'after'  => '</div>',
		) );

}

//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'outreach_author_box_gravatar_size' );
function outreach_author_box_gravatar_size( $size ) {

    return '80';
    
}

//* Remove comment form allowed tags
add_filter( 'comment_form_defaults', 'outreach_remove_comment_form_allowed_tags' );
function outreach_remove_comment_form_allowed_tags( $defaults ) {
	
	$defaults['comment_notes_after'] = '';
	return $defaults;

}

//* Add the sub footer section
add_action( 'genesis_before_footer', 'outreach_sub_footer', 5 );
function outreach_sub_footer() {

	if ( is_active_sidebar( 'sub-footer-left' ) || is_active_sidebar( 'sub-footer-right' ) ) {
		echo '<div class="sub-footer"><div class="wrap">';
		
		   genesis_widget_area( 'sub-footer-left', array(
		       'before' => '<div class="sub-footer-left">',
		       'after'  => '</div>',
		   ) );
	
		   genesis_widget_area( 'sub-footer-right', array(
		       'before' => '<div class="sub-footer-right">',
		       'after'  => '</div>',
		   ) );
	
		echo '</div><!-- end .wrap --></div><!-- end .sub-footer -->';	
	}
	
}

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'outreach' ),
	'description' => __( 'This is the top section of the Home page.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home - Bottom', 'outreach' ),
	'description' => __( 'This is the bottom section of the Home page.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'after-entry',
	'name'        => __( 'After Entry', 'outreach' ),
	'description' => __( 'This is the after entry widget area.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'sub-footer-left',
	'name'        => __( 'Sub Footer - Left', 'outreach' ),
	'description' => __( 'This is the left section of the sub footer.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'sub-footer-right',
	'name'        => __( 'Sub Footer - Right', 'outreach' ),
	'description' => __( 'This is the right section of the sub footer.', 'outreach' ),
) );



add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {    
    
    wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'jquery-ui-tabs' );
    wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/js/custom-script.js', array( 'jquery' ) );
//    wp_enqueue_script( 'smoothscroll-script', get_stylesheet_directory_uri() . '/js/scg_smooth_scrolling.js', array( 'jquery' ) );
//    wp_enqueue_script( 'smoothscroll-script', get_stylesheet_directory_uri() . '/js/script.js', array( 'jquery' ) );
//    wp_enqueue_script( 'smoothscroll-script', get_stylesheet_directory_uri() . '/js/scg_smooth_scrolling.js', array( 'jquery' ) );
    //Add Bootstrap Js
        wp_enqueue_script( 'bootstrap-script', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
        
        
        /******** Smooth Scroll js *********/
         wp_enqueue_script( 'smooth-scroll-script', get_stylesheet_directory_uri() . '/js/script.js', array( 'jquery' ) );
         
         
        /******Slick Slider******/
        
        
//        wp_enqueue_script( 'slickslider-script', get_stylesheet_directory_uri() . '/js/slick.min.js', array( 'jquery' ) );
//        wp_enqueue_style( 'slickslider-style', get_stylesheet_directory_uri(). '/css/slick-theme.css' );
//        wp_enqueue_style( 'slickslidernew-style', get_stylesheet_directory_uri(). '/css/slick.css' );
        
        
        // Loads Bootstrap Styleshett
	wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri(). '/css/bootstrap.min.css' );
        wp_enqueue_style( 'bootstrap-style-map', get_stylesheet_directory_uri(). '/css/bootstrap.css.map' );
        wp_enqueue_style( 'bootstrap-theme-style', get_stylesheet_directory_uri(). '/css/bootstrap-theme.min.css' );
        wp_enqueue_style( 'bootstrap-theme-map', get_stylesheet_directory_uri(). '/css/bootstrap-theme.css.map' );
        //Add Jquery Cycle2 Js
//        wp_enqueue_script( 'cycle2', get_stylesheet_directory_uri() . '/js/Cycle2.js', array( 'jquery' ) );
//        wp_enqueue_script( 'cycle2-old', get_stylesheet_directory_uri() . '/js/jquery.cycle2.js', array( 'jquery' ) );
//        wp_enqueue_script( 'cycle2-new', get_stylesheet_directory_uri() . '/js/cycle2.corousel.js', array( 'jquery' ) );
}

/*========Custom Sidebar============*/
genesis_register_sidebar( array(
	'id'          => 'ja-header-left',
	'name'        => __( 'Header  Left', 'outreach' ),
	'description' => __( 'This is the left section of the Header.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'ja-header-right',
	'name'        => __( 'Header  Right New', 'outreach' ),
	'description' => __( 'This is the left section of the Header.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'ja-header-title',
	'name'        => __( 'Header  Title Area', 'outreach' ),
	'description' => __( 'This is the left section of the Header.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'midsection-one',
	'name'        => __( 'Home Featured Items', 'outreach' ),
	'description' => __( 'This is the left section of the Header.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'midsection-two',
	'name'        => __( 'Home Product Category section', 'outreach' ),
	'description' => __( 'This is the left section of the Header.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'midsection-three',
	'name'        => __( 'Home Newslatter section', 'outreach' ),
	'description' => __( 'This is the left section of the Header.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'midsection-four',
	'name'        => __( 'Home Recommended Section', 'outreach' ),
	'description' => __( 'This is the middle section of Home.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'midsection-five',
	'name'        => __( 'Home Contact Section', 'outreach' ),
	'description' => __( 'This is the Contact section of Home.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'midsection-six',
	'name'        => __( 'Home Footer Section', 'outreach' ),
	'description' => __( 'This is the Footer Section of Home.', 'outreach' ),
) );
add_action('genesis_header', 'add_content_to_header');
function add_content_to_header() { ?>
    <div class="ja-headr row">
    <div class="ja-headr-left col-md-6">
        <?php dynamic_sidebar('ja-header-left'); ?>
    </div>    
        <div class="ja-headr-right col-md-6">
        <?php dynamic_sidebar('ja-header-right'); ?>
    </div>   
        </div>    
        <div class="ja-title col-md-12">
        <?php dynamic_sidebar('ja-header-title'); ?>
    </div>     
<?php }

add_action('woocommerce_after_shop_loop_item', 'get_star_rating' );
function get_star_rating()
{
    global $woocommerce, $product;
    $average = $product->get_average_rating();

    echo '<div class="star-rating new-edit"><span style="width:'.( ( $average / 5 ) * 100 ) . '%"><strong itemprop="ratingValue" class="rating">'.$average.'</strong> '.__( 'out of 5', 'woocommerce' ).'</span></div>';
}

//* Customize the entire footer
remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'sp_custom_footer' );
function sp_custom_footer() {
    dynamic_sidebar('midsection-six');
}
add_filter( 'wp_nav_menu_items', 'theme_menu_extras', 10, 2 );
function theme_menu_extras( $menu, $args ) {
    if ( 'primary' !== $args->theme_location )
		return $menu;
         ob_start();
	get_search_form();
	$search = ob_get_clean();
	$menu  .= '<li class="right search">' . $search . '</li>';
        return $menu;
}

add_filter( 'genesis_search_text', 'sp_search_text' );
function sp_search_text( $text ) {
   return esc_attr( 'Search here...' );
}
/*********woocommerce filters and hooks start*************/
