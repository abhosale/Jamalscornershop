<?php
/**
 * This file adds the Home Page to the Outreach Pro Theme.
 *
 * @author StudioPress
 * @package Outreach Pro
 * @subpackage Customizations
 */

add_action( 'genesis_meta', 'outreach_home_genesis_meta' );
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */
function outreach_home_genesis_meta() {

	if ( is_active_sidebar( 'home-top' ) || is_active_sidebar( 'home-bottom' ) ) {

		//* Force full-width-content layout setting
		add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
		
		//* Add outreach-pro-home body class
		add_filter( 'body_class', 'outreach_body_class' );
		
		//* Remove breadcrumbs
		remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

		//* Remove the default Genesis loop
		remove_action( 'genesis_loop', 'genesis_do_loop' );
		
		//* Add home top widgets
		add_action( 'genesis_loop', 'outreach_home_top_widgets' );

		//* Add home bottom widgets
		add_action( 'genesis_before_footer', 'outreach_home_bottom_widgets', 1 );

	}
        
        if ( is_active_sidebar( 'midsection-one' ) ) {
            add_action( 'genesis_before_footer', 'outreach_midsection_one', 2 );
        }
         if ( is_active_sidebar( 'midsection-two' ) ) {
            add_action( 'genesis_before_footer', 'outreach_midsection_two', 3 );
        }
         if ( is_active_sidebar( 'midsection-three' ) ) {
            add_action( 'genesis_before_footer', 'outreach_midsection_three', 4 );
        }
        if ( is_active_sidebar( 'midsection-four' ) ) {
            add_action( 'genesis_before_footer', 'outreach_midsection_four', 5 );
        }
        if ( is_active_sidebar( 'midsection-five' ) ) {
            add_action( 'genesis_before_footer', 'outreach_midsection_five', 6 );
        }
//          if ( is_active_sidebar( 'midsection-six' ) ) {
//            add_action( 'genesis_before_footer', 'outreach_midsection_six', 6 );
//        }
//        

}

function outreach_body_class( $classes ) {

	$classes[] = 'outreach-pro-home';
	return $classes;
	
}

function outreach_home_top_widgets() {

	genesis_widget_area( 'home-top', array(
		'before' => '<div class="home-top widget-area">',
		'after'  => '</div>',
	) );
	
}

function outreach_home_bottom_widgets() {
	
	genesis_widget_area( 'home-bottom', array(
		'before' => '<div class="home-bottom widget-area"><div class="wrap">',
		'after'  => '</div></div>',
	) );

}

function outreach_midsection_one() {
	
	genesis_widget_area( 'midsection-one', array(
		'before' => '<div class="midsection-one widget-area"><div class="wrap">',
		'after'  => '</div></div>',
	) );

}
function outreach_midsection_two() {
	
	genesis_widget_area( 'midsection-two', array(
		'before' => '<div class="midsection-two widget-area"><div class="wrap">',
		'after'  => '</div></div>',
	) );

}
function outreach_midsection_three() {
	
	genesis_widget_area( 'midsection-three', array(
		'before' => '<div class="midsection-three widget-area"><div class="wrap">',
		'after'  => '</div></div>',
	) );

}
function outreach_midsection_four() {
	
	genesis_widget_area( 'midsection-four', array(
		'before' => '<div class="midsection-four widget-area"><div class="wrap">',
		'after'  => '</div></div>',
	) );

}
function outreach_midsection_five() {
	
	genesis_widget_area( 'midsection-five', array(
		'before' => '<div class="midsection-five widget-area"><div class="wrap">',
		'after'  => '</div></div>',
	) );

}
//function outreach_midsection_six() {
//	
//	genesis_widget_area( 'midsection-six', array(
//		'before' => '<div class="midsection-six widget-area"><div class="wrap">',
//		'after'  => '</div></div>',
//	) );
//
//}
genesis();
