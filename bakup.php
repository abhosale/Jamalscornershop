<h3>School-facilities</h3>
<div class="border"></div>
<p>1. Organized Internal Departments to Serve Parents <br />
2. Educational Bag Project for Pre-school Stage (Kindergarten)<br />
3. Surveillance Cameras in all Classrooms, Yards and Corridors<br />
4. Learning Resources Centre equipped with Latest Teaching Aids, Books and References in Arabic and English serving the Curriculum<br />
5. Computer Special Centers for IT Subjects<br />
6. Electrical Lifts with Automatic Control System <br />
7. School Health Clinic with a Resident Nurse and an Official Authorization form the Ministry of Health<br />
8. An atelier for fine arts and a special room to develop musical skills<br />
9. Use of Smart Board as a Teaching Means<br />
10. Indoor and Outdoor Modern and Spacious Playgrounds for Kindergarten and other Educational Stages<br />
11. Modern Scientific Laboratories (Physics &ndash; Chemistry &ndash; Biology)<br />
12. Spacious and Comfortable Classrooms<br />
13. School Bus Service</p>